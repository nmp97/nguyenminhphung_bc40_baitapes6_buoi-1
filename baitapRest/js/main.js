// function Sum
var sumAverage = (...args) => {
  var sum = 0;
  args.forEach((arg) => {
    sum += arg;
  });
  return sum / args.length;
};

// get value from form

var scoreAverage1 = () => {
  var mathScore = document.getElementById('inpToan').value * 1;
  var physicScore = document.getElementById('inpLy').value * 1;
  var chemistryScore = document.getElementById('inpHoa').value * 1;

  document.getElementById('tbKhoi1').innerHTML = `Điểm trung bình: ${sumAverage(
    mathScore,
    physicScore,
    chemistryScore
  ).toFixed(1)}`;
};

var scoreAverage2 = () => {
  var LiteratureScore = document.getElementById('inpVan').value * 1;
  var historyScore = document.getElementById('inpSu').value * 1;
  var GeographyScore = document.getElementById('inpDia').value * 1;
  var englishScore = document.getElementById('inpEnglish').value * 1;

  document.getElementById('tbKhoi2').innerHTML = `Điểm trung bình: ${sumAverage(
    LiteratureScore,
    historyScore,
    GeographyScore,
    englishScore
  ).toFixed(1)}`;
};
