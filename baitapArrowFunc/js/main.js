var colorList = [
  'pallet',
  'viridian',
  'pewter',
  'cerulean',
  'vermillion',
  'lavender',
  'celadon',
  'saffron',
  'fuschia',
  'cinnabar',
];

var renderColor = (arr) => {
  let contentHTML = '';
  arr.forEach((item) => {
    let contentColor = `
                        <button class="color-button ${item}" onclick="changeColor('${item}')"></button>
                     `;
    contentHTML += contentColor;
  });
  document.getElementById('colorContainer').innerHTML = contentHTML;
  document.querySelectorAll('.color-button')[0].classList.add('active');
};

renderColor(colorList);

var changeColor = (color) => {
  // Remove color and active
  document.getElementById('house').classList.remove(...colorList);
  document.querySelectorAll('.color-button').forEach((item) => {
    item.classList.remove('active');
  });
  // Add color and active
  document.getElementById('house').classList.add(color);
  document.querySelector(`.color-button.${color}`).classList.add('active');
};
